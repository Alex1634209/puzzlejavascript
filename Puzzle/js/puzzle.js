document.addEventListener("DOMContentLoaded",alert("Write a unique username to be able to play the game"));


var thePuzzleBoard=[[null,null,null],[null,null,null],[null,null,null]];
var nberMoves=0;

function tileArray(){
  var tds=document.getElementsByTagName("td");
  var test = tds.length;
  var counter=0;
  for(var i=0;i<thePuzzleBoard.length;i++){
    for (var y=0;y<thePuzzleBoard[i].length;y++){
      var tempNum = tds[counter].innerHTML;
      if (tempNum == ""){
        thePuzzleBoard[i][y] = 0;
        counter++;
      } else{
        //convert it from string to int
       thePuzzleBoard[i][y] = parseInt(tempNum);
       counter++;
     }
    }//end nested for loop
  }//end for loop
}//end function tileArray

function resetTileArrayAndNberMoves(){//resets array values back to null
  for (var i=0;i<thePuzzleBoard.length;i++){
    for(var y=0;y<thePuzzleBoard[i].length;y++){
      thePuzzleBoard[i][y]=null;
    }//end nested for loop
  }//end for loop
  nberMoves=0;
}//end resetTileArray


  function swap2Tiles(indexTile1,indexTile2){
    //the changes are made on the array representing the board
    //and the the board itself for the user to see
   var i1=0; var y1=0; var i2=0; var y2=0;
     for (var i=0;i<thePuzzleBoard.length;i++){
       for (var y=0;y<thePuzzleBoard[i].length;y++){
         if (indexTile1==thePuzzleBoard[i][y]){
           i1=i; y1=y;
         }else if (indexTile2==thePuzzleBoard[i][y]){
          i2=i; y2=y;
         }//end if statement
       }
     }//end for loop

     //change the 2D array
     var temp = thePuzzleBoard[i1][y1];
     thePuzzleBoard[i1][y1] = thePuzzleBoard[i2][y2];
     thePuzzleBoard[i2][y2] = temp;

     //retrieve the two tds on the board itself
     var tds = document.getElementsByTagName("td");
     var tdTemp1; var tdTemp2;
     for (i=0;i<tds.length;i++){
       if (tds[i].innerHTML == thePuzzleBoard[i1][y1]){
         tdTemp1=tds[i];
       } else if (tds[i].innerHTML == thePuzzleBoard[i2][y2]){
         tdTemp2=tds[i];
       }//end if statement
     }//end for loop
     //change the contet of two tiles by taking the content and the className,
     // then display it for the user to see
     var tdTempIH = tdTemp1.innerHTML;
     var tdTempCN = tdTemp1.className;
     tdTemp1.innerHTML = tdTemp2.innerHTML;
     tdTemp1.className = tdTemp2.className;
     tdTemp2.innerHTML = tdTempIH;
     tdTemp2.className = tdTempCN;
     nberMoves++;
     document.getElementById("moves").value = nberMoves;
  }//end function swap2Tiles

  function getNeighboursIndicesArr(arrayIndex){
    var neighborArr=[]; var arrayUP=0; var arrayRIGHT=0;
    var arrayDOWN=0; var arrayLEFT=0;
    for (var i=0;i<thePuzzleBoard.length;i++){
      for (var y=0;y<thePuzzleBoard[i].length;y++){
        if (thePuzzleBoard[i][y] == arrayIndex){
          try{
           arrayUP=thePuzzleBoard[i-1][y];
            neighborArr.push(arrayUP);
         } catch (error){
           arrayUP=-1;
            neighborArr.push(arrayUP);
         }
         try{
           arrayRIGHT=thePuzzleBoard[i][y+1];
           neighborArr.push(arrayRIGHT);
         } catch(error){
           arrayRIGHT=-1;
            neighborArr.push(arrayRIGHT);
         }
         try{
           arrayDOWN=thePuzzleBoard[i+1][y];
            neighborArr.push(arrayDOWN);
         } catch(error){
           arrayDOWN=-1;
            neighborArr.push(arrayDOWN);
         }
         try{
           arrayLEFT=thePuzzleBoard[i][y-1];
            neighborArr.push(arrayLEFT);
         } catch(error){
           arrayLEFT=-1;
            neighborArr.push(arrayLEFT);
         }
        }//end if statement
      }//end nested for loop
    }//end for loop
    return neighborArr;
  }//end function getNeighboursIndicesArr


  function processClickTile(arrayIndex){
   //This is where the the mving of tiles will be processed
   //and see if the game continues or not
   var arrayOfNeighbours = getNeighboursIndicesArr(arrayIndex);
   //Test out if its a wrong move or not for audio
   var wrongCounter=0;
   for (var counter=0;counter<arrayOfNeighbours.length;counter++){
     if (arrayOfNeighbours[counter] != 0){wrongCounter++;}
   }//end for loop
   if (wrongCounter == 4){
     playAudio();
   }
   for (var i=0;i<arrayOfNeighbours.length;i++){
     if (arrayOfNeighbours[i]==0){
       swap2Tiles(0,arrayIndex);
     }
   }//end for loop
   var counterNotSuccess = computeNumberMisplaced();
   if(counterNotSuccess==0){
     var theStatus = "success";
     terminateGame(theStatus);
   }
  }//end function processClickTile

  function computeNumberMisplaced(){
    var arrayToCompare=[[1,2,3],[4,5,6],[7,8,0]];
   var counterNCP=0;
   for (var i=0;i<thePuzzleBoard.length;i++){
     for (var y=0;y<thePuzzleBoard[i].length;y++){
       if (arrayToCompare[i][y]!=thePuzzleBoard[i][y]){
         counterNCP++;
       }
     }
   }//end for loop
   return counterNCP;
  }//end function computeNumberMisplaced
