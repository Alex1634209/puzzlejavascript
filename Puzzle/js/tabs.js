document.addEventListener("DOMContentLoaded",alert("lets do this"));

var seconds = 0;  var minutes = 0; var hours = 0;
var timer;

function initialPage(gameId){
  //hide the game until player clicks play button
  if (gameId='checkBoardId'){
    document.getElementById(gameId).style.display="none";
    document.getElementById('levelFour').disabled=true;
    document.getElementById('levelFive').disabled=true;
  }
}//end function initialPage
 function Utility(){

 }//end function for object Utility


function generateRandomNumber(minValue,maxValue){
  /*generate a random number between minValue and maxValue. Will be
  used by PuzleGame to create the board initial random state */
  var randomNumArray = [null,null,null,null,null,null,null,null,null];
  while(minValue<=maxValue){
    //Fill array with numbers from 0 to 8 at random places
    var numIsInArray=false;
    //generate random numbers between 0 and 8
    var randomNum = Math.floor(Math.random()*9);
    for (var i=0;i<randomNumArray.length;i++){
      //Measure too avoid repeating numbers
      if (randomNumArray[i]==randomNum){
        numIsInArray=true;
        break;
      }//end if statement
    }//end for loop
    if (!numIsInArray){
      randomNumArray[minValue]=randomNum;
      minValue++;
    }//else do nothing and repeat whole process
  }//end while loop
return randomNumArray;
}//end generateRandomNumber


function playWinnerAudio(){
  var audioWinner = new Audio("sounds/Firework.mp3");
  audioWinner.play();
}//end function playWinneraudio


function playAudio(){
  //Play sound whenever wrong move is made
  var wrongMoveAudio = new Audio("sounds/beep-07.mp3");
  wrongMoveAudio.play();
}//end function playAudio

function sampleEasyBoardTests(){
  var randomNumArray = generateRandomNumber(0,8);
  var tiles = document.getElementsByTagName("td");
  for (var y=0;y<tiles.length;y++){
    if (randomNumArray[y]==0){
      tiles[y].className = 'emptyTile';
      tiles[y].innerHTML = '';
    } else{
      tiles[y].className = 'filledTile';
      tiles[y].innerHTML = randomNumArray[y];
    } //end if atatement
  }//end for loop
}//end sampleEasyBoardTests


function appendTableStats(theStatus){
  var statTR = document.createElement("TR");
  var nameTH = document.createElement("TH");
  var nameTN = document.createTextNode(document.getElementById('playerName').value);
  nameTH.appendChild(nameTN);
  var levelTH = document.createElement("TH");
  var level = document.getElementById("puzzleLevelId").value;
  var levelTN = document.createTextNode(level+"X"+level);
  levelTH.appendChild(levelTN);
  var numMovesTH = document.createElement("TH");
  var numMovesTN = document.createTextNode(document.getElementById("moves").value);
  numMovesTH.appendChild(numMovesTN);
  var statusTH = document.createElement("TH");
  var statusTN = document.createTextNode(theStatus);
  statusTH.appendChild(statusTN);
  var durationTH = document.createElement("TH");
  var secs= document.getElementById("sec").value;
  var mins = document.getElementById("min").value;
  var hours = document.getElementById("hrs").value;
  var durationTN = document.createTextNode(hours+":"+mins+":"+secs);
  durationTH.appendChild(durationTN);
  statTR.appendChild(nameTH); statTR.appendChild(levelTH);
  statTR.appendChild(numMovesTH); statTR.appendChild(statusTH); statTR.appendChild(durationTH);
  document.getElementById("tableStatsID").appendChild(statTR);
}


function terminateGame(theStatus){
appendTableStats(theStatus);
  if (theStatus == "cancelled"){
     resetTileArrayAndNberMoves();
     document.getElementById('checkBoardId').style.display="none";
     document.getElementById('playerName').value="";
     document.getElementById('playerName').disabled=false;
     document.getElementById('puzzleLevelId').disabled=false;
     clearInterval(timer);
     seconds=0;minutes=0;hours=0;
     //Make all the readOnly inputs values go back to zero
     var backToZero = document.getElementsByClassName("progressIn");
     for (var i=0;i<backToZero.length;i++){
       backToZero[i].value="0";
       backToZero[i].style.color="#C0C0C0";
     }//end for loop
  }else {//otherwise if it is a success
    resetTileArrayAndNberMoves();
    playWinnerAudio();
    document.getElementById('checkBoardId').style.display="none";
    document.getElementById('playerName').value="";
    document.getElementById('playerName').disabled=false;
    document.getElementById('puzzleLevelId').disabled=false;
    clearInterval(timer);
    seconds=0;minutes=0;hours=0;
    //Make all the readOnly inputs values go back to zero
    var backToZero = document.getElementsByClassName("progressIn");
    for (var i=0;i<backToZero.length;i++){
      backToZero[i].value="0";
      backToZero[i].style.color="#C0C0C0";
    }//end for loop
  }//end if statement
}//end function terminateGame


function getInfo() {
  var player=document.getElementById("playerName").value;
  var numMoves=document.getElementById("moves").value;
} //end function statsOrInfo

function statsOrInfo(firstTab,secondTab){
  //changes from stats or info, depending on what user clicks
  var firstid = document.getElementById(firstTab);
  var secondid = document.getElementById(secondTab);
  firstid.style.display="block";
  secondid.style.display="none";
}//end function infoOrStats

function checkFormFilled(){
  //if user didn't write a username, then he can't click play yet
  //if written, then button is enabled and changed to green
  var inputText=document.getElementById("playerName").value;
  if (inputText!=''){
    enableButton("buttonPlay",false,'greenButton');
  } else if (inputText=='') {
    enableButton("buttonPlay",true,'disableButton');
  }
}//end function checkFormFilled

function enableButton(btnId, theStatus, btnClass){
   document.getElementById(btnId).disabled=theStatus;
   document.getElementById(btnId).className=btnClass;
}//end enableButton


function mainProgram(){
  //The bulk of the assignment will go into this function
  //First, let's change button to red and disable input for username and dimension choice
  enableButton("buttonCancel",false,'redButton');
  document.getElementById('checkBoardId').style.display="block";
  document.getElementById('buttonPlay').disabled=true;
  document.getElementById('playerName').disabled=true;
  document.getElementById('puzzleLevelId').disabled=true;
  sampleEasyBoardTests();
  tileArray();
  timer = setInterval(showChrono,1000);
  //Get read only inputs and make the text go from gray to black
  var readInput = document.getElementsByClassName("progressIn");
  for (var i=0;i<readInput.length;i++){
    readInput[i].style.color="#000000";
  }

}//end function playGame

function cancelPuzzlePlay(){
  //If player decides to quit game, it will bring him back like in the main menu
  var theStatus = "cancelled";
  terminateGame(theStatus);
  enableButton("buttonCancel",true,'disableButton');
  enableButton("buttonPlay",true,'disableButton');
}//end function cancelCurrentGame

function showChrono(){
   seconds++;
   document.getElementById("sec").value=seconds;
   if (seconds==60){
     seconds=0;
     minutes++;
     document.getElementById("min").value=minutes;
     if (minutes==60){
       minutes=0;
       hours++;
       document.getElementById("hrs").value=hours;
     }//end nested if
   }//end if
}//end function showChrono


/*function playerManager(){
  var listPlayers=[]; var gameCounter=1;
  var gameDuration=0; var nberMoves=0;
  function storeGameStats(theStatus){
     var nameofPlayer = document.getElementById('playerName').value;
     var levelOfPuzzle = document.getElementById('puzzleLevelId').value;
     var numMoves=document.getElementById("moves").value;
     var aPlayer = new Player(gameCounter,nameofPlayer,levelOfPuzzle,theStatus,nberMoves,gameDuration);
     listPlayers.push(aPlayer);
     gameCounter++; nberMoves=0; gameDuration=0;
  }
}//end function playerManager*/


/*function init() {
  /*It does the following after DOM is fully loaded:
    instantiate a Utility object called utilityObj and a PlayerManager object called playerManager.
    attach an event listener to the button play that will call mainProgram once clicked.
    attach an event listener to the button cancel that will call cancelPuzzlePlay once clicked.
    attach an event listeners to player name, game level: to check whether the input are filled or not(calls checkFormFilled).*/
  /*var utilityObj = new Utility();
  var playerManager = new PlayerManager();
}*/
